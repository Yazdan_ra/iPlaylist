from telegram import Update, ReplyKeyboardMarkup, InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import CommandHandler, CallbackContext, ConversationHandler, MessageHandler, Filters

keyboard = ReplyKeyboardMarkup([
    [
        'Get Playlist',
        'Create Playlist'
    ],
    [
        'Support us!',
    ],
    [
        'HOME',
    ],
])

inline_keyboard = [
    [
        InlineKeyboardButton(text='Yazdan', url='https://t.me/yazdan_ra'),
    ],
    [
        InlineKeyboardButton(text='Nima', url='https://t.me/nima10khodaveisi'),
    ],
]


def support(update: Update, context: CallbackContext):
    context.user_data.clear()
    chat_id = update.message.from_user.id

    markup = InlineKeyboardMarkup(inline_keyboard)

    context.bot.send_message(chat_id, ('First Thank you!\n' +
                                       'you have any idea? please share with us!\n' +
                                       'just click and contact us! ^__-'),
                             reply_markup=markup)

    context.bot.send_message(chat_id, 'Choose one!', reply_markup=keyboard)

    return ConversationHandler.END


HANDLER = ConversationHandler(
    entry_points=[
        CommandHandler('contact', support),
        MessageHandler(Filters.text('Contact us!'), support),
    ],

    states={

    },

    fallbacks=[

    ],

    allow_reentry=True,
)
