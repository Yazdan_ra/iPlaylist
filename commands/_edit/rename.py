from telegram import Update, ReplyKeyboardMarkup, ReplyKeyboardRemove
from telegram.ext import CallbackContext, ConversationHandler, CommandHandler, MessageHandler, Filters

from config import ADMIN
from models import User, Playlist

NAME, NEWNAME = range(2)

end_keyboard = ReplyKeyboardMarkup([
    [
        'Get Playlist',
        'Add Song',
    ],
    [
        'Delete Playlist',
        'Create Playlist',
    ],
    [
        'HOME',
    ],
])


def check_playlist_exists(update, name):
    chat_id = update.message.from_user.id

    user = User.select().where(User.chat_id == chat_id)
    playlist = Playlist.select().where((Playlist.user == user) & (Playlist.name == name))
    if not len(playlist):
        return True

    return False


def start_rename(update: Update, context: CallbackContext):
    context.user_data.clear()

    chat_id = update.message.from_user.id

    try:
        if len(context.args):
            name = context.args[0]
            new_name = context.args[1]

            if check_playlist_exists(update, name):
                context.bot.send_message(chat_id, 'This Playlist dose not exist! Enter again:')
                return ConversationHandler.END

            context.user_data['name'] = name
            context.user_data['new_name'] = new_name

            return rename(update, context)

    except:
        pass

    user = User.select().where(User.chat_id == chat_id)[0]
    playlists = Playlist.select().where(Playlist.user == user)

    keyboard = ReplyKeyboardMarkup([[playlist.name] for playlist in playlists], resize_keyboard=True)

    context.bot.send_message(chat_id,
                             ('Enter the name of the playlist you wanna rename!\n\n' +
                              '/cancel to cancel the process'),
                             reply_markup=keyboard)

    return NAME


def get_name(update, context):
    chat_id = update.message.from_user.id
    name = update.message.text

    # check playlist exists
    if check_playlist_exists(update, name):
        context.bot.send_message(chat_id, 'This Playlist dose not exist! Enter again:')
        return

    context.bot.send_message(chat_id, ('Now send the new name:\n\n' +
                                       '/cancel to cancel the process'),
                             reply_markup=ReplyKeyboardRemove())

    context.user_data['name'] = name

    return NEWNAME


def get_new_name(update, context):
    new_name = update.message.text

    context.user_data['new_name'] = new_name

    return rename(update, context)


def rename(update, context):
    chat_id = update.message.from_user.id

    name = context.user_data['name']
    new_name = context.user_data['new_name']

    try:
        user = User.select().where(User.chat_id == chat_id)
        Playlist.update({
            Playlist.name: new_name
        }).where((Playlist.user == user) & (Playlist.name == name)).execute()

        context.bot.send_message(chat_id, 'Playlist *{}* renamed to *{}* successfully!'.format(name, new_name),
                                 parse_mode='Markdown',
                                 reply_markup=end_keyboard)

    except Exception as err:
        if chat_id in ADMIN:
            context.bot.send_message(chat_id, err)
        return error(update, context)

    context.user_data.clear()
    return ConversationHandler.END


def error(update, context):
    chat_id = update.message.chat.id
    context.bot.delete_message(chat_id, update.message.message_id)
    context.bot.send_message(chat_id, ('Oops...\n' +
                                       'Something went wrong! Try again:'))
    return


def cancel(update, context):
    chat_id = update.message.from_user.id
    context.bot.send_message(chat_id, 'Process was canceled!', reply_markup=end_keyboard)

    context.user_data.clear()
    return ConversationHandler.END


HANDLER = ConversationHandler(
    entry_points=[
        CommandHandler('rename', start_rename),
        MessageHandler(Filters.text('Rename Playlist'), start_rename),
    ],

    states={

        NAME: [MessageHandler(Filters.text, get_name)],

        NEWNAME: [MessageHandler(Filters.text, get_new_name)],

    },

    fallbacks=[
        MessageHandler(Filters.command, cancel),
        MessageHandler(Filters.all, error)
    ],

    allow_reentry=True,
)
